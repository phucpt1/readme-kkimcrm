# Kha Kim CRM Application Document

## Table of contents

* [Installation](#installation)
* [Connect to Database](#connect-to-database)
* [Config Chat Server](#config-chat-server)
    * [Setup Facebook Application](#setup-facebook-application)
    * [login.phtml](#loginphtml)
    * [index.phtml](#indexphtml)
    * [BaseController.php](#basecontrollerphp)
    * [WebHookController.php](#webhookcontrollerphp)
    * [Setup socket nodejs](#start-socket-to-receive-and-send-message)
* [FAQ (Frequently Asking Question)](#FAQ)

## Installation

### Prerequisites

Clone the app on repo

```
git clone git@bitbucket.org:pixiostudiovn/crm-khakim.git
```

Go to the project directory and run composer install


```
composer install
```

## __Connect to Database__

Create database name `crmkkim` and import data from `khakimcrm-test.sql`


```
mysql crmkkim < khakimcrm-test.sql
```

Go inside `config/autoload` and change setting of 2 files:

* doctrine.local.php (MYSQL ORM connection)
* local.php (Jago session configuration)

Copy 2 files from .example

```
cp doctrine.local.php.example doctrine.local.php && cp local.php.example local.php
```

Setup the following config

`doctrine.local.php`
```
// doctrine.local.php

'host'     => '127.0.0.1',
'port'     => '3306',
'user'     => '<your-mysql-user>',
'password' => '<your-mysql-password>',
'dbname'   => 'crmkkim',
```

`local.php`
```
// local.php

'hostname'  => '127.0.0.1',
'username'  => '<your-mysql-user>',
'password'  => '<your-mysql-password>',
'database'  => 'crmkkim',
```

## __Config Chat Server__

### Setup Facebook Application

What will you need from facebook:

* Create a Facebook Account (or use your own)
* Create a fanpage with you as the admin of that page
* Create a Facebook Application from [developer dashboard](https://developers.facebook.com/apps) or watch [Facebook sets tutorial]()
* AppId
* App Secret
* AuthToken

### **Warning**  `root-dir` WILL BE RELATED to __crm-khakim__ and WILL CONTAINT like the following

```
/crm-khakim
.
├── bin
├── composer.json
├── composer.lock
├── config
├── data
├── init_autoloader.php
├── kkimcrm_backup
├── module
├── public
├── README.md
├── server-chat
├── source
└── vendor
```

Check and Copy these 3 files if not exist

* [login.phtml](#loginphtml) - applicatioin/fanpage-manage
* [index.phtml](#indexphtml) - j-fanpage
* [WebhookController.php](#webhookcontrollerphp)

This last file just need to be edit

* [BaseController.php](#basecontrollerphp)

**Warning**: All of the following files can be found in `kkimcrm_backup`.

---

### login.phtml

check and copy this file from kkimcrm_backup

* __login.phtml__ in [`root-dir`](#warning-root-dir-will-be-related-to-crm-khakim-and-will-containt-like-the-following)/module/Application/view/application/fanpage-manage/login.phtml

__copy command__

```bash
cp kkimcrm_backup/application/fanpage-manage/login.phtml module/Application/view/application/fanpage-manage/
```

#### Edit The file

Find These lines of code

```javascript
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0&appId=175053016409311&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
```

Edit The `version=5.0` and `appId` in the `js.src`

> js.src = httpss://connect.facebook.net/vi_VN/sdk.js#xfbml=1&`version=v5.0`&`appId=<your-facebook-appId>`&autoLogAppEvents=1;

__Note__: version must be 5.0

---

### index.phtml

Check and copy this file from kkimcrm_backup

* __index.phtml__ in [`root-dir`](#warning-root-dir-will-be-related-to-crm-khakim-and-will-containt-like-the-following)/module/JFanpage/view/j-fanpage/index/index.phtml

__copy command__

```
cp kkimcrm_backup/jfanpage/index/index.phtml module/JFanpage/view/j-fanpage/index/
```

#### Edit the file

Near the end of the file. `line 266`

```
<?php $this->headScript()->captureStart() ?>
	// FB settting
	var fbAppId = "1041824602849626";
	var fbAppVer = "v5.0";
	
	// other
	var uColor = "<?php echo $user->getColor() ?>";
```

---

### BaseController.php

At the beginning of the file. Look for theses: `line 32`

```
    protected $em;
    protected $filter;
    const APP_ID = '1041824602849626';
    const APP_SECRET = '4f3e760327216ec183c667fa3705104e';
    const DEFAULT_GRAPH_VERSION = 'v5.0';
    const TAG_CHUA_MUA_HANG = 'ChuaMuaHang';
    const TAG_CHUA_MUA_HANG_ID = 24;
    const TAG_DA_MUA_HANG = 'DaMuaHang';
    const TAG_DA_MUA_HANG_ID = 23;
    const SEND_TYPING_ON = 'on';
    
```

Edit `APP_ID`, `APP_SECRET` and check `DEFAULT_GRAPH_VERSION` = __v5.0__

> const APP_ID = '`your-app-id`';
> 
> const APP_SECRET = '`your-app-secret`';
> 
> const DEFAULT_GRAPH_VERSION = '`v5.0`';

---

### WebhookController.php

* __WebhookController.php__ in [`root-dir`](#warning-root-dir-will-be-related-to-crm-khakim-and-will-containt-like-the-following)/module/Application/src/Application/Controller/

__copy command__
```
cp kkimcrm_backup/WebhookController.php module/Application/src/Application/Controller/
```

#### Edit the file

At the beginning of the file, look for `DOMAIN` and `PORT`, `line 32`.

```
const DOMAIN = 'https://phucphungdev.xyz';
const PORT   = '3000';
```

change domain name to your `https` domain name.

> const DOMAIN = '`your-https-domain-name`';
>
> const PORT   = '3000';

__Warning:__ The domain name must be `https` otherwise the Facebook API won't work.

Next, search for `callback_url` at `line 63`.

```javascript
$response = $fb->post(
    '/' . self::APP_ID . '/subscriptions',
        array(
                'object' => 'page',
                'callback_url' => 'https://phucphungdev.xyz/dowebhook',
                'fields' => 'messages, feed',
                //'fields' => 'messages, conversations, feed',
                'include_values' => 'true',
                'verify_token' => 'jago_verify_token',
        ),
    $appToken
);
```
Edit `callback_url` to your `https` domain name.

> 'callback_url' => '`your-https-domain-name`'

### Start Socket to receive and send message

Go to `server-chat`

```
cd server-chat
```

#### Edit index.js

at `line 4`

```
var options = {
  key: fs.readFileSync('/home/brody/privkey.pem'),
  cert: fs.readFileSync('/home/brody/fullchain.pem'),
  requestCert: false,
  rejectUnauthorized: false
};
```

`comment out` the 2 properties, if you have them just edit to direct to them

> key: fs.readFileSync('/home/brody/privkey.pem'),
> 
> cert: fs.readFileSync('/home/brody/fullchain.pem'),

Run the command


```
// forever start absolute path to the index.js

forever start /home/brody/crm-khakim/server-chat/index.js
```

Check if it is running

```
forever list
```

```
brody@ubuntu-s-1vcpu-1gb-sgp1-01:~$ forever list
info:    Forever processes running
data:        uid  command                                          script                                      forever pid   id logfile                       uptime                                
data:    [2] 6j4H node /home/brody/crm-khakim/server-chat/index.js 23498   23505    /home/brody/.forever/6j4H.log 0:8:33:6.72899999999936 
```

## FAQ

